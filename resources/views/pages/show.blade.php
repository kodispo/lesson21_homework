@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">{{ $page->title }}</h1>
    <p class="h3">{{ $page->intro }}</p>
@endsection

@section('content')
    {{ $page->content }}
@endsection
