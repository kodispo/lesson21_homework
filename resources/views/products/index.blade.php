@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase text-center">Products</h1>
    <a href="/products/create" class="btn btn-outline-primary">Add new product</a>
@endsection

@section('content')
    @include('partials.alert')

    @if($products)
        <div class="card-deck mb-3 text-center">
        @foreach($products as $product)
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{ $product->title }}</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">${{ $product->price }}</h1>
                    <div class="mb-3">{{ \Illuminate\Support\Str::limit($product->description, 130, $end='...') }}</div>
                    <div class="d-flex flex-md-row justify-content-center">
                        <a href="/products/{{ $product->id }}" class="btn btn-sm btn-block m-0 btn-outline-primary">View</a>
                        <a href="/products/{{ $product->id }}/edit" class="btn btn-sm btn-block m-0 btn-outline-primary">Edit</a>
                        <form action="/products/{{$product->id}}" method="post" class="d-block">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-block m-0 btn-outline-primary">Remove</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    @endif
@endsection
