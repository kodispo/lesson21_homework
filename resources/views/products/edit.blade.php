@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">Edit product</h1>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            @include('partials.errors')
            <form action="/products/{{ $product->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" name="title" id="title" value="{{ $product->title }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" id="slug" value="{{ $product->slug }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" name="price" id="price" value="{{ $product->price }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" id="description" class="form-control" rows="5">{{ $product->description }}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
