@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">{{ $product->title }}</h1>
    <p class="h3">${{ $product->price }}</p>
@endsection

@section('content')
    {{ $product->description }}
@endsection
