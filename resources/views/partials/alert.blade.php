@if(Session::has('success'))
    <div class="alert alert-success mb-4 text-center" role="alert">
        {{ Session::get('success') }}
    </div>
@endif
