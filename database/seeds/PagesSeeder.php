<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'title' => 'Shopping Help',
                'slug' => 'shopping-help',
                'intro' => 'Check order status, track or change your order, return items and more.',
                'content' => 'We want you to have the simplest, easiest buying experience possible. But we know you might have a few questions. Read on for details about purchasing, shipping, checking order status, returns, and more.',
            ],
            [
                'title' => 'Accessibility',
                'slug' => 'accessibility',
                'intro' => 'Technology is most powerful when it empowers everyone.',
                'content' => 'Taking a family portrait. Catching up over FaceTime. Raising the blinds to let in the morning light. We want everyone to enjoy the everyday moments that technology helps make possible, so we work to make every Apple product accessible from the very start. Because the true value of a device isn’t measured by how powerful it is, but by how much it empowers you.',
            ],
            [
                'title' => 'Contact Us',
                'slug' => 'contact-us',
                'intro' => 'Find store hours and contact information for all locations.',
                'content' => 'Apple.com is a convenient place to purchase Apple products and accessories from Apple and other manufacturers. You can buy online or call (800) MY–APPLE (800–692–7753). You can get information about an order you placed on the Apple Online Store through the Order Status page. If you prefer, you can also get order status or make changes by phone at (800) 692–7753.',
            ],
        ]);
    }
}
