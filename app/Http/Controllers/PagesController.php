<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('pages.index')->with(compact('pages'));
    }

    public function show(Page $page)
    {
        return view('pages.show')->with(compact('page'));
    }

    public function create()
    {
        return view('pages.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);
        Page::create($request->all());
        return redirect('/pages/')->with('success', 'Created successfully');
    }

    public function edit(Page $page)
    {
        return view('pages.edit', compact('page'));
    }

    public function update(Page $page, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);
        $page->update($request->all());
        return redirect('/pages/')->with('success', 'Updated successfully');
    }

    public function destroy(Page $page)
    {
        $page->delete();
        return redirect('/pages/')->with('success', 'Removed successfully');
    }
}
