<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with(compact('products'));
    }

    public function show(Product $product)
    {
        return view('products.show')->with(compact('product'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
        ]);
        Product::create($request->all());
        return redirect('/')->with('success', 'Created successfully');
    }

    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    public function update(Product $product, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
        ]);
        $product->update($request->all());
        return redirect('/')->with('success', 'Updated successfully');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/')->with('success', 'Removed successfully');
    }
}
